package com.geoquiz.view.menu;

import com.geoquiz.view.button.*;
import com.geoquiz.view.utility.Background;

import com.geoquiz.view.label.MyLabel;
import com.geoquiz.view.label.MyLabelFactory;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * The scene where user can choose game modality.
 */
public class ModeScene extends AbstractScene {

    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 350;
    private static final double POS_3_Y = 200;
    private static final double POS_Y_BACK = 600;
    private static final double POS_1_X = 100;
    private static final double LABEL_FONT = 35;
    private static final double BUTTON_WIDTH = 350;
    private static final double OPACITY = 0.5;
    private static final double USER_LABEL_FONT = 40;

    private static final Text BUTTONPRESSED = new Text();

    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public ModeScene(final Stage mainStage) {
        super(mainStage);

        final MyLabel userLabel = MyLabelFactory.createMyLabel("USER: " + LoginMenuScene.getUsername(), Color.BLACK,
                USER_LABEL_FONT);

        final MyButton back = MyButtonFactory.createMyButton(Buttons.INDIETRO.toString(), Color.BLUE, BUTTON_WIDTH);
        final MyButton classic = MyButtonFactory.createMyButton(ButtonsMode.CLASSICA.toString(), Color.BLUE, BUTTON_WIDTH);
        final MyButton challenge = MyButtonFactory.createMyButton(ButtonsMode.SFIDA.toString(), Color.BLUE, BUTTON_WIDTH);
        final MyButton training = MyButtonFactory.createMyButton(ButtonsMode.ALLENAMENTO.toString(), Color.BLUE, BUTTON_WIDTH);

        String message = "";
        switch (CategoryScene.getCategoryPressed())
        {
            case CAPITALI:
                message = "Sai indicare la capitale di ciascun paese?";
                break;
            case MONUMENTI:
                message = "Sai indicare dove si trovano questi famosi monumenti?";
                break;
            case BANDIERE:
                message = "Sai indicare i paesi in base alla bandiera nazionale?";
                break;
            case CUCINA:
                message = "Sai indicare di quali paesi sono tipici questi piatti?";
                break;
            case VALUTE:
                message = "Sai indicare qual e' la valuta adottata da ciascun paese?";
                break;
        }
        message += "\nScegli prima la modalità di gioco!";
        Label label = new Label();
        label.setText(message);

        label.setFont(Font.font("Italic", FontWeight.BOLD, LABEL_FONT));

        VBox vbox = new VBox();
        vbox.setTranslateX(POS_2_X);
        vbox.setTranslateY(POS_2_Y);
        vbox.getChildren().addAll((Node) classic, (Node) challenge, (Node) training);
        VBox vbox2 = new VBox();
        vbox2.getChildren().add((Node) back);
        VBox vbox3 = new VBox();
        vbox3.getChildren().add(label);

        vbox2.setTranslateX(POS_1_X);
        vbox2.setTranslateY(POS_Y_BACK);
        vbox3.setTranslateX(POS_2_X);
        vbox3.setTranslateY(POS_3_Y);

        ((Node) back).setOnMouseClicked(event -> clickHandler(null, mainStage, CategoryScene.class));
        ((Node) classic).setOnMouseClicked(event -> {
            final ButtonsMode category = ButtonsMode.CLASSICA;
            switch (CategoryScene.getCategoryPressed())
            {
                case CAPITALI:
                case MONUMENTI:
                    clickHandler(category, mainStage, LevelScene.class);
                default:
                    clickHandler(category, mainStage, QuizGamePlay.class);
            }
        });
        ((Node) challenge).setOnMouseClicked(event -> clickHandler(ButtonsMode.SFIDA, mainStage, QuizGamePlay.class));
        ((Node) training).setOnMouseClicked(event -> clickHandler(ButtonsMode.ALLENAMENTO, mainStage, QuizGamePlay.class));

        Pane panel = new Pane();
        panel.getChildren().addAll(ModeScene.createBackgroundImage(), Background.createBackground(),
                Background.getLogo(), vbox, vbox2, vbox3, (Node) userLabel);

        this.setRoot(panel);
    }

    private void clickHandler(ButtonsMode category, Stage stage, Class<? extends Scene> scene)
    {
        super.ClickHandler(stage, scene);

        if (category != null) {
            BUTTONPRESSED.setText(category.toString());
        }
    }

    /**
     * @return game modality.
     */
    static ButtonsMode getModalityPressed() {
        return ButtonsMode.valueOf(BUTTONPRESSED.getText());
    }

    /**
     * @return background image for own category.
     */
    static ImageView createBackgroundImage() {
        switch (CategoryScene.getCategoryPressed())
        {
            case MONUMENTI:
            case VALUTE:
            case CUCINA:
            case BANDIERE:
                final String categoryName = CategoryScene.getCategoryPressed().toString().toLowerCase();
                final ImageView img = Background.getCategoryImage("/images/" + categoryName + ".jpg");
                img.setOpacity(OPACITY);
                return img;
            default:
                return Background.getImage();
        }
    }

}
