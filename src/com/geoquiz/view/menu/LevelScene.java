package com.geoquiz.view.menu;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import com.geoquiz.view.button.*;
import com.geoquiz.view.label.MyLabel;
import com.geoquiz.view.label.MyLabelFactory;
import com.geoquiz.view.utility.Background;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * The scene where user can choose difficulty level.
 */
public class LevelScene extends AbstractScene {

    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 300;
    private static final double POS_Y_BACK = 600;
    private static final double POS_1_X = 100;
    private static final double BUTTON_WIDTH = 350;
    private static final double USER_LABEL_FONT = 40;

    private static final Text BUTTONPRESSED = new Text();

    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public LevelScene(final Stage mainStage) {
        super(mainStage);

        final MyLabel userLabel = MyLabelFactory.createMyLabel("USER: " + LoginMenuScene.getUsername(), Color.BLACK,
                USER_LABEL_FONT);

        final MyButton back;
        final MyButton easy;
        final MyButton medium;
        final MyButton hard;

        back = MyButtonFactory.createMyButton(Buttons.INDIETRO.toString(), Color.BLUE, BUTTON_WIDTH);
        easy = MyButtonFactory.createMyButton(ButtonsDifficulty.FACILE.toString(), Color.BLUE, BUTTON_WIDTH);
        medium = MyButtonFactory.createMyButton(ButtonsDifficulty.MEDIO.toString(), Color.BLUE, BUTTON_WIDTH);
        hard = MyButtonFactory.createMyButton(ButtonsDifficulty.DIFFICILE.toString(), Color.BLUE, BUTTON_WIDTH);

        VBox vbox = new VBox();
        vbox.getChildren().addAll((Node) easy, (Node) medium, (Node) hard);
        VBox vbox2 = new VBox();
        vbox2.getChildren().add((Node) back);

        vbox.setTranslateX(POS_2_X);
        vbox.setTranslateY(POS_2_Y);

        vbox2.setTranslateX(POS_1_X);
        vbox2.setTranslateY(POS_Y_BACK);

        ((Node) back).setOnMouseClicked(event -> ClickHandler(mainStage, CategoryScene.class));
        ((Node) easy).setOnMouseClicked(event -> ClickHandler(ButtonsDifficulty.FACILE, mainStage));
        ((Node) medium).setOnMouseClicked(event -> ClickHandler(ButtonsDifficulty.MEDIO, mainStage));
        ((Node) hard).setOnMouseClicked(event -> ClickHandler(ButtonsDifficulty.DIFFICILE, mainStage));

        Pane panel = new Pane();
        panel.getChildren().addAll(ModeScene.createBackgroundImage(), Background.createBackground(), vbox, vbox2,
                Background.getLogo(), (Node) userLabel);

        this.setRoot(panel);
    }
    private void ClickHandler(ButtonsDifficulty category, Stage stage)
    {
        super.ClickHandler(stage, QuizGamePlay.class);

        if (category != null) {
            BUTTONPRESSED.setText(category.toString());
        }
    }

    /**
     * @return difficulty level.
     */
    public static String getLevelPressed() {
        return BUTTONPRESSED.getText();
    }

}
