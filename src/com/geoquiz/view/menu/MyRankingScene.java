package com.geoquiz.view.menu;

import java.io.IOException;
import java.util.Map;

import javax.xml.bind.JAXBException;

import com.geoquiz.utility.Pair;
import com.geoquiz.view.button.ButtonsCategory;

import com.geoquiz.view.button.ButtonsDifficulty;
import com.geoquiz.view.button.ButtonsEnum;
import com.geoquiz.view.button.ButtonsMode;
import javafx.stage.Stage;

/**
 * The statistics scene where user can see own records.
 */
public class MyRankingScene extends AbsoluteRankingScene {

    private Map<Pair<String, String>, Integer> map;

    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public MyRankingScene(final Stage mainStage) {
        super(mainStage);
        super.getTitle().setText("My records");
        try {
            map = super.getRanking().getPersonalRanking(LoginMenuScene.getUsername());
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        super.clearLabel();

        super.getCapitalsEasy().setText(super.getCapitalsEasy().getText()
                + this.getRecordByCategory(ButtonsCategory.CAPITALI, ButtonsDifficulty.FACILE));
        super.getCapitalsMedium().setText(super.getCapitalsMedium().getText()
                + this.getRecordByCategory(ButtonsCategory.CAPITALI, ButtonsDifficulty.MEDIO));
        super.getCapitalsHard().setText(super.getCapitalsHard().getText()
                + this.getRecordByCategory(ButtonsCategory.CAPITALI, ButtonsDifficulty.DIFFICILE));
        super.getCapitalsChallenge().setText(super.getCapitalsChallenge().getText()
                + this.getRecordByCategory(ButtonsCategory.CAPITALI, ButtonsMode.SFIDA));
        super.getMonumentsEasy().setText(super.getMonumentsEasy().getText()
                + this.getRecordByCategory(ButtonsCategory.MONUMENTI, ButtonsDifficulty.FACILE));
        super.getMonumentsMedium().setText(super.getMonumentsMedium().getText()
                + this.getRecordByCategory(ButtonsCategory.MONUMENTI, ButtonsDifficulty.MEDIO));
        super.getMonumentsHard().setText(super.getMonumentsHard().getText()
                + this.getRecordByCategory(ButtonsCategory.MONUMENTI, ButtonsDifficulty.DIFFICILE));
        super.getMonumentsChallenge().setText(super.getMonumentsChallenge().getText()
                + this.getRecordByCategory(ButtonsCategory.MONUMENTI, ButtonsMode.SFIDA));
        super.getFlagsClassic().setText(super.getFlagsClassic().getText()
                + this.getRecordByCategory(ButtonsCategory.BANDIERE, ButtonsMode.CLASSICA));
        super.getFlagsChallenge().setText(super.getFlagsChallenge().getText()
                + this.getRecordByCategory(ButtonsCategory.BANDIERE, ButtonsMode.SFIDA));
        super.getCurrenciesClassic().setText(super.getCurrenciesClassic().getText()
                + this.getRecordByCategory(ButtonsCategory.VALUTE, ButtonsMode.CLASSICA));
        super.getCurrenciesChallenge().setText(super.getCurrenciesChallenge().getText()
                + this.getRecordByCategory(ButtonsCategory.VALUTE, ButtonsMode.SFIDA));
        super.getDishesClassic().setText(super.getDishesClassic().getText()
                + this.getRecordByCategory(ButtonsCategory.CUCINA, ButtonsMode.CLASSICA));
        super.getDishesChallenge().setText(super.getDishesChallenge().getText()
                + this.getRecordByCategory(ButtonsCategory.CUCINA, ButtonsMode.SFIDA));

    }

    private String getRecordByCategory(final ButtonsCategory category, final ButtonsEnum difficulty) {
        final Integer record = this.map.get(new Pair<>(category.toString(), difficulty.toString()));
        return record == null ? "" : record.toString();
    }
}