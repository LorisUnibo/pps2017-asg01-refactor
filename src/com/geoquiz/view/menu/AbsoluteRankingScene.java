package com.geoquiz.view.menu;

import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;

import com.geoquiz.controller.ranking.Ranking;
import com.geoquiz.utility.Pair;
import com.geoquiz.view.button.*;
import com.geoquiz.view.label.MyLabel;
import com.geoquiz.view.label.MyLabelFactory;
import com.geoquiz.view.utility.Background;

import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import static com.geoquiz.view.button.ButtonsDifficulty.*;
import static com.geoquiz.view.button.ButtonsMode.CLASSICA;
import static com.geoquiz.view.button.ButtonsMode.SFIDA;

/**
 * The ranking scene where user can see other user's records.
 */
public class AbsoluteRankingScene extends AbstractScene {

    private static final double POS_1_X = 20;
    private static final double POS_Y_BACK = 650;
    private static final double FONT = 35;
    private static final double BUTTON_WIDTH = 350;
    private static final double FONT_MODE = 25;
    private static final double POS_X_CATEGORY_BOX = 50;
    private static final double POS_Y_CATEGORY_BOX = 200;
    private static final double POS_X_CATEGORY_BOX_2 = 650;
    private static final double POS_Y_CATEGORY_BOX_2 = 75;
    private static final double POS_X_CAPITALS_BOX = 300;
    private static final double POS_Y_CAPITALS_BOX = 205;
    private static final double POS_X_MONUMENTS_BOX = 300;
    private static final double POS_Y_MONUMENTS_BOX = 450;
    private static final double POS_X_FLAGS_BOX = 850;
    private static final double POS_Y_FLAGS_BOX = 80;
    private static final double POS_X_CURRENCIES_BOX = 850;
    private static final double POS_Y_CURRENCIES_BOX = 325;
    private static final double POS_X_DISHES_BOX = 850;
    private static final double POS_Y_DISHES_BOX = 570;
    private static final double TITLE_FONT = 95;
    private static final String S = " -> ";

    private final MyLabel title;

    private static final int EASY = 0;
    private static final int MEDIUM = 1;
    private static final int HARD = 2;
    //private static final int CHALLENGE = 3;
    private static final int CLASSIC = 0;
    //private static final int CHALLENGE_C = 1;

    private List<MyLabel> capitalsLabels;
    private List<MyLabel> monumentsLabels;
    private List<MyLabel> currenciesLabels;
    private List<MyLabel> flagsLabels;
    private List<MyLabel> dishesLabels;

    private final Ranking ranking = Ranking.getInstance();

    private Map<Pair<String, String>, Pair<String, Integer>> map;

    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public AbsoluteRankingScene(final Stage mainStage) {
        super(mainStage);

        final MyLabel capitals;
        final MyLabel monuments;
        final MyLabel flags;
        final MyLabel currencies;
        final MyLabel dishes;

        final MyButton indietro;

        title = MyLabelFactory.createMyLabel("Global records", Color.BLACK, TITLE_FONT);
        try {
            map = this.ranking.getGlobalRanking();
        } catch (ClassNotFoundException | IOException e1) {
            e1.printStackTrace();
        }

        indietro = MyButtonFactory.createMyButton(Buttons.INDIETRO.toString(), Color.BLUE, BUTTON_WIDTH);

        capitals = MyLabelFactory.createMyLabel(ButtonsCategory.CAPITALI.toString(), Color.RED, FONT);
        monuments = MyLabelFactory.createMyLabel(ButtonsCategory.MONUMENTI.toString(), Color.RED, FONT);
        flags = MyLabelFactory.createMyLabel(ButtonsCategory.BANDIERE.toString(), Color.RED, FONT);
        currencies = MyLabelFactory.createMyLabel(ButtonsCategory.VALUTE.toString(), Color.RED, FONT);
        dishes = MyLabelFactory.createMyLabel(ButtonsCategory.CUCINA.toString(), Color.RED, FONT);

        VBox titleBox = new VBox();
        titleBox.getChildren().add((Node) title);

        capitalsLabels = CreateLabels(ButtonsCategory.CAPITALI, false);
        monumentsLabels = CreateLabels(ButtonsCategory.MONUMENTI, false);
        currenciesLabels = CreateLabels(ButtonsCategory.VALUTE, true);
        flagsLabels = CreateLabels(ButtonsCategory.BANDIERE, true);
        dishesLabels = CreateLabels(ButtonsCategory.CUCINA, true);

        VBox capitalsBox = new VBox();
        VBox monumentsBox = new VBox();
        VBox currenciesBox = new VBox();
        VBox flagsBox = new VBox();
        VBox dishesBox = new VBox();

        capitalsBox.getChildren().addAll(LabelsToNode(capitalsLabels));
        monumentsBox.getChildren().addAll(LabelsToNode(monumentsLabels));
        currenciesBox.getChildren().addAll(LabelsToNode(currenciesLabels));
        flagsBox.getChildren().addAll(LabelsToNode(flagsLabels));
        dishesBox.getChildren().addAll(LabelsToNode(dishesLabels));

        VBox categoryBox = new VBox(200);
        VBox categoryBox2 = new VBox(200);

        categoryBox.getChildren().addAll((Node) capitals, (Node) monuments);
        categoryBox2.getChildren().addAll((Node) flags, (Node) currencies, (Node) dishes);

        categoryBox.setTranslateX(POS_X_CATEGORY_BOX);
        categoryBox.setTranslateY(POS_Y_CATEGORY_BOX);
        categoryBox2.setTranslateX(POS_X_CATEGORY_BOX_2);
        categoryBox2.setTranslateY(POS_Y_CATEGORY_BOX_2);
        capitalsBox.setTranslateX(POS_X_CAPITALS_BOX);
        capitalsBox.setTranslateY(POS_Y_CAPITALS_BOX);
        monumentsBox.setTranslateX(POS_X_MONUMENTS_BOX);
        monumentsBox.setTranslateY(POS_Y_MONUMENTS_BOX);
        dishesBox.setTranslateX(POS_X_DISHES_BOX);
        dishesBox.setTranslateY(POS_Y_DISHES_BOX);
        flagsBox.setTranslateX(POS_X_FLAGS_BOX);
        flagsBox.setTranslateY(POS_Y_FLAGS_BOX);
        currenciesBox.setTranslateX(POS_X_CURRENCIES_BOX);
        currenciesBox.setTranslateY(POS_Y_CURRENCIES_BOX);

        VBox vbox = new VBox();
        vbox.setTranslateX(POS_1_X);
        vbox.setTranslateY(POS_Y_BACK);
        vbox.getChildren().add((Node) indietro);

        ((Node) indietro).setOnMouseClicked(event -> ClickHandler(mainStage, MainMenuScene.class));

        Pane panel = new Pane();
        panel.getChildren().addAll(Background.getImage(), Background.createBackground(), vbox, categoryBox,
                categoryBox2, capitalsBox, monumentsBox, flagsBox, currenciesBox, dishesBox, titleBox);

        this.setRoot(panel);
    }

    private List<MyLabel> CreateLabels(ButtonsCategory category, boolean OnlyClassicalChallenge){
        List<MyLabel> labels = new ArrayList<>();

        java.util.function.Consumer<ButtonsEnum> create = difficulty ->
                labels.add(MyLabelFactory.createMyLabel(difficulty.toString() + S
                        + this.getRecordbyCategory(category.toString(), difficulty.toString()),
                Color.BLACK, FONT_MODE));

        if (!OnlyClassicalChallenge){
            Arrays.asList(FACILE, MEDIO, DIFFICILE, SFIDA).forEach(create);
        } else {
            Arrays.asList(CLASSICA, SFIDA).forEach(create);
        }
        return labels;
    }

    private List<Node> LabelsToNode(List<MyLabel> labels) {
        List<Node> result = new ArrayList<>();
        if (labels.size() != 2) {
            Collections.addAll(result,
                    (Node)GetEasy(labels),
                    (Node)GetMedium(labels),
                    (Node)GetHard(labels),
                    (Node)GetChallenge(labels));
        } else {
            Collections.addAll(result,
                    (Node)GetClassic(labels),
                    (Node)GetChallenge(labels));
        }
        return result;
    }

    private String getRecordbyCategory(final String category, final String difficulty) {
        final Pair<String, Integer> record = this.map.get(new Pair<>(category, difficulty));
        return record == null ? "" : record.getX() + " " + "(" + record.getY() + ")";
    }

    /**
     * clear records labels.
     */
    protected void clearLabel() {
        Consumer<MyLabel> clear = label -> label.setText(label.getText().substring(0,
                label.getText().indexOf(" ", label.getText().indexOf(" ") + 1) + 1));

        capitalsLabels.forEach(clear);
        monumentsLabels.forEach(clear);
        flagsLabels.forEach(clear);
        currenciesLabels.forEach(clear);
        dishesLabels.forEach(clear);
    }

    /**
     * gets the value of title label.
     * 
     * @return title label.
     */
    protected MyLabel getTitle() {
        return title;
    }

    /**
     * gets the value of facilecap label.
     * 
     * @return facilecap label.
     */
    protected MyLabel getCapitalsEasy() {
        return GetEasy(capitalsLabels);
    }

    /**
     * gets the value of mediocap label.
     * 
     * @return mediocap label.
     */
    protected MyLabel getCapitalsMedium() {
        return GetMedium(capitalsLabels);
    }

    /**
     * gets the value of difficilecap label.
     * 
     * @return difficilecap label.
     */
    protected MyLabel getCapitalsHard() {
        return GetHard(capitalsLabels);
    }

    /**
     * gets the value of sfidacap label.
     * 
     * @return sfidacap label.
     */
    protected MyLabel getCapitalsChallenge() {
        return GetChallenge(capitalsLabels);
    }

    /**
     * gets the value of facilemon label.
     * 
     * @return facilemon label.
     */
    protected MyLabel getMonumentsEasy() {
        return GetEasy(monumentsLabels);
    }

    /**
     * gets the value of mediomon label.
     * 
     * @return mediomon label.
     */
    protected MyLabel getMonumentsMedium() {
        return GetMedium(monumentsLabels);
    }

    /**
     * gets the value of difficilemon label.
     * 
     * @return difficilemon label.
     */
    protected MyLabel getMonumentsHard() {
        return GetHard(monumentsLabels);
    }

    /**
     * gets the value of sfidamon label.
     * 
     * @return sfidamon label.
     */
    protected MyLabel getMonumentsChallenge() {
        return GetChallenge(monumentsLabels);
    }

    /**
     * gets the value of classicaban label.
     * 
     * @return classicaban label.
     */
    protected MyLabel getFlagsClassic() {
        return GetClassic(flagsLabels);
    }

    /**
     * gets the value of sfidaban label.
     * 
     * @return sfidaban label.
     */
    protected MyLabel getFlagsChallenge() {
        return GetChallenge(flagsLabels);
    }

    /**
     * gets the value of classicaval label.
     * 
     * @return classicaval.
     */
    protected MyLabel getCurrenciesClassic() {
        return GetClassic(currenciesLabels);
    }

    /**
     * gets the value of sfidaval label.
     * 
     * @return sfidaval label.
     */
    protected MyLabel getCurrenciesChallenge() {
        return GetChallenge(currenciesLabels);
    }

    /**
     * gets the value of classicacuc label.
     * 
     * @return classicacuc label.
     */
    protected MyLabel getDishesClassic() {
        return GetClassic(dishesLabels);
    }

    /**
     * gets the value of sfidacuc label.
     * 
     * @return sfidacuc label.
     */
    protected MyLabel getDishesChallenge() {
        return GetChallenge(dishesLabels);
    }

    private MyLabel GetEasy(List<MyLabel> labels) {
        return labels.get(EASY);
    }
    private MyLabel GetMedium(List<MyLabel> labels) {
        return labels.get(MEDIUM);
    }
    private MyLabel GetHard(List<MyLabel> labels) {
        return labels.get(HARD);
    }
    private MyLabel GetChallenge(List<MyLabel> labels) {
        return labels.get(labels.size() - 1);
    }
    private MyLabel GetClassic(List<MyLabel> labels) {
        return labels.get(CLASSIC);
    }

    /**
     * gets the controller.
     * 
     * @return controller.
     */
    protected Ranking getRanking() {
        return this.ranking;
    }
}
