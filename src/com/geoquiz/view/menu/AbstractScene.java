package com.geoquiz.view.menu;

import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.lang.reflect.InvocationTargetException;

public abstract class AbstractScene extends Scene {
    public AbstractScene(final Stage mainStage) {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());
    }

    protected void PlayClickIfPossible() {
        if (!MainWindow.isWavDisabled()) {
            MainWindow.playClick();
        }
    }
    /**
     * Click handler that uses the specified scene
     * @param category the category to be written
     * @param stage the stage where the scene is called
     * @param scene the scene to call
     */
    protected void ClickHandler(Stage stage, Class<? extends Scene> scene)
    {
        PlayClickIfPossible();
        try {
            stage.setScene(scene.getConstructor(Stage.class).newInstance(stage));
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
