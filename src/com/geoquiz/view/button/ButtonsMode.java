package com.geoquiz.view.button;

/**
 * The menu's buttons who a player can press to choose modality.
 */
public enum ButtonsMode implements ButtonsEnum  {
    CLASSICA, SFIDA, ALLENAMENTO
}
