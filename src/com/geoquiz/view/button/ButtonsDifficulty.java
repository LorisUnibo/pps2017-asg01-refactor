package com.geoquiz.view.button;

/**
 * The menu's buttons who a player can press to choose difficulty
 */
public enum ButtonsDifficulty implements ButtonsEnum  {
    /**
     * Represents the difficulty level "Facile".
     */
    FACILE,
    /**
     * Represents the difficulty level "Medio".
     */
    MEDIO,
    /**
     * Represents the difficulty level "Difficile".
     */
    DIFFICILE
}
